# LocalStack + Terraform

## Launch localstack

```
$ ./run-localstack
```
Note:

- script will deploy only Lamba, SQS and S3
- IAM is disabled by default
- access_key and secret_key are set to "test"
- endpoint is http://localhost:4566

## Terraform

Reminder:

- `terraform init` will download provider (and externals modules if required)
- `terraform plan` will help to refresh status and prepare `apply` step
- `terraform apply -auto-approve` to create resource
