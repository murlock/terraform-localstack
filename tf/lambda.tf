resource "aws_lambda_function" "my-function" {
  function_name = "my-first-function"
  filename = "function/my-function.zip"
  handler = "function.handler"
  # TODO implement IAM
  role = ""
  runtime = "python3.7"
  # used to trigger update, can we force creation of zip file in terraform ?
  source_code_hash = filebase64sha256("function/my-function.zip")

  depends_on = [
    # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function#cloudwatch-logging-and-permissions
    # TODO when enabled, terraform detect a error on loop cycle
    # Error: Cycle: aws_cloudwatch_log_group.example, aws_lambda_function.my-function
    # but documentation refers to it
    # aws_cloudwatch_log_group.example,
    # TODO IAM
    # aws_iam_role_policy_attachment.lambda_logs,
  ]
}

resource "aws_lambda_event_source_mapping" "sds-to-lambda" {
  event_source_arn = aws_sqs_queue.myqueue.arn
  function_name    = aws_lambda_function.my-function.arn
}