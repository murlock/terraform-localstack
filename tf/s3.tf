resource "aws_s3_bucket" "mybucket" {
  bucket = "startup-chronicles-demo-account"
}

resource "aws_s3_bucket_notification" "bucket_notif" {
  # reference bucket above
  bucket = aws_s3_bucket.mybucket.id
  queue {
    queue_arn     = aws_sqs_queue.myqueue.arn
    events        = ["s3:ObjectCreated:*"]
  }
}
