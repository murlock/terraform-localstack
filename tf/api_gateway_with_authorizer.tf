# Variables
#variable "myregion" {
#  type = string
#  default = "us-east-1"
#}
#
#variable "accountId" {
#  type = string
#  default = "000000000000"
#}

# API Gateway
resource "aws_api_gateway_rest_api" "auth-api" {
  name = "my-auth-api"
}

resource "aws_api_gateway_resource" "auth-resource" {
  path_part   = "authenticated"
  parent_id   = aws_api_gateway_rest_api.auth-api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.auth-api.id
}

# IT WORKS
resource "aws_api_gateway_method" "auth-method" {
  rest_api_id   = aws_api_gateway_rest_api.auth-api.id
  resource_id   = aws_api_gateway_resource.auth-resource.id
  http_method   = "GET"
  #authorization = "NONE"
  authorization = "CUSTOM"
  request_validator_id = aws_lambda_function.my-auth-function.id
  request_parameters = {
    "method.request.header.X-Authorization" = true
  }
}

resource "aws_api_gateway_integration" "auth-integration" {
  # The API Gateway instance
  rest_api_id             = aws_api_gateway_rest_api.auth-api.id
  # The PATH
  resource_id             = aws_api_gateway_resource.auth-resource.id
  # Method
  http_method             = aws_api_gateway_method.auth-method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.my-auth-function.invoke_arn
}

resource "aws_api_gateway_deployment" "auth-deployment" {
  depends_on = [aws_api_gateway_integration.integration]

  rest_api_id = aws_api_gateway_rest_api.auth-api.id
  stage_name  = "test"

  variables = {
    "answer" = "42"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Lambda
#resource "aws_lambda_permission" "apigw_lambda" {
#  statement_id  = "AllowExecutionFromAPIGateway"
#  action        = "lambda:InvokeFunction"
#  function_name = aws_lambda_function.my-function.function_name
#  principal     = "apigateway.amazonaws.com"
#
#  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
#  source_arn = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.resource.path}"
#}


# Lambda is available with
# curl http://localhost:4566/restapis/{REST_API_ID}/{STAGE}/_user_request_/{RESOURCE}/


# example

# curl -v -H "X-Authorization: unauthorized" http://localhost:4566/restapis/70jhzekmxd/test/_user_request_/authenticated/
# > GET /restapis/70jhzekmxd/test/_user_request_/authenticated/ HTTP/1.1
# > Host: localhost:4566
# > User-Agent: curl/7.68.0
# > Accept: */*
# > X-Authorization: unauthorized
# >
# < HTTP/1.1 401
# < content-type: text/html; charset=utf-8
# < content-length: 0
# < access-control-allow-origin: *
# < access-control-allow-methods: HEAD,GET,PUT,POST,DELETE,OPTIONS,PATCH
# < access-control-allow-headers: authorization,content-type,content-md5,cache-control,x-amz-content-sha256,x-amz-date,x-amz-security-token,x-amz-user-agent,x-amz-target,x-amz-acl,x-amz-version-id,x-localstack-target,x-amz-tagging
# < access-control-expose-headers: x-amz-version-id
# < connection: close
# < date: Sun, 08 Nov 2020 11:24:55 GMT
# < server: hypercorn-h11
#
#
# curl -v -H "X-Authorization: allow" http://localhost:4566/restapis/70jhzekmxd/test/_user_request_/authenticated/
# > GET /restapis/70jhzekmxd/test/_user_request_/authenticated/ HTTP/1.1
# > Host: localhost:4566
# > User-Agent: curl/7.68.0
# > Accept: */*
# > X-Authorization: allow
# >
# * Mark bundle as not supporting multiuse
# < HTTP/1.1 200
# < content-type: text/html; charset=utf-8
# < content-length: 107
# < access-control-allow-origin: *
# < access-control-allow-methods: HEAD,GET,PUT,POST,DELETE,OPTIONS,PATCH
# < access-control-allow-headers: authorization,content-type,content-md5,cache-control,x-amz-content-sha256,x-amz-date,x-amz-security-token,x-amz-user-agent,x-amz-target,x-amz-acl,x-amz-version-id,x-localstack-target,x-amz-tagging
# < access-control-expose-headers: x-amz-version-id
# < connection: close
# < date: Sun, 08 Nov 2020 11:25:48 GMT
# < server: hypercorn-h11
# <
# {"principalId": "user", "context": {"stringKey": "some random stuff", "numberKey": 42, "booleanKey": true}}%
