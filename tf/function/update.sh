#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd "$DIR" || exit

rm -f my-function.zip
zip my-function.zip function.py


rm -f my-authorizer.zip
zip my-authorizer.zip authorizer.py
