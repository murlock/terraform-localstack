#!/usr/bin/env python

# some docs
# https://docs.aws.amazon.com/fr_fr/apigateway/latest/developerguide/apigateway-use-lambda-authorizer.html

def generatePolicy(principalId, effect, resource):
    authResponse = {
        "principalId": principalId,
    }
    policyDocument = {}
    policyDocument['Version'] = "2012-10-17"
    policyDocument['Statement'] = [{
        "Action": "execute-api:Invoke",
        "Effect": effect,
        "Resource": resource,
    }]
    authResponse['context'] = {
        "stringKey": "some random stuff",
        "numberKey": 42,
        "booleanKey": True,
    }
    return authResponse


def handler(event, context):
    #
    token = event.get("headers", {}).get('X-Authorization', None)
    statusCode = 200
    policy = ""
    if token == "allow":
        policy = generatePolicy("user", "allow", event.get("methodArn", "invalid"))
    elif token == "deny":
        policy = generatePolicy("user", "deny", event.get("methodArn", "invalid"))
    elif token == "unauthorized":
        statusCode = 401
    else:
        statusCode = 500

    # must return:
    # - a IAM policy
    # - an ID
    return {
        'statusCode': statusCode,
        'body': policy
    }