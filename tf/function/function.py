# FIXME context_client doesn't work with localstack
# I have to test this function on AWS with this context
# echo "eyJtc2ciOiAiaGVsbG9fd29ybGQifQ==" | base64 -d | jq
# {
#   "msg": "hello_world"
# }
# aws --endpoint http://localhost:4566 lambda invoke \
#   --log-type tail \
#   --function-name my-first-function \
#   --client-context 'eyJtc2ciOiAiaGVsbG9fd29ybGQifQ==' \
#   out
#
# the payload seems ok:
# aws --endpoint http://localhost:4566 lambda invoke \
#   --log-type tail \
#   --function-name my-first-function \
#   --payload '{"payload": "toto"}' \
#   out; cat out | jq

# example from API Gateway
# {
#   "event": {
#     "path": "/resource42",
#     "headers": {
#       "Remote-Addr": "10.10.0.1",
#       "Host": "localhost:4566",
#       "User-Agent": "curl/7.68.0",
#       "Accept": "*/*",
#       "X-Forwarded-For": "10.10.0.1, localhost:4566, 127.0.0.1, localhost:4566",
#       "x-localstack-edge": "https://localhost:4566",
#       "Authorization": "AWS4-HMAC-SHA256 Credential=LocalStackDummyAccessKey/20160623/us-east-1/apigateway/aws4_request, SignedHeaders=content-type;host;x-amz-date;x-amz-target, Signature=1234"
#     },
#     "multiValueHeaders": {
#       "Remote-Addr": [
#         "10.10.0.1"
#       ],
#       "Host": [
#         "localhost:4566"
#       ],
#       "User-Agent": [
#         "curl/7.68.0"
#       ],
#       "Accept": [
#         "*/*"
#       ],
#       "X-Forwarded-For": [
#         "10.10.0.1, localhost:4566, 127.0.0.1, localhost:4566"
#       ],
#       "x-localstack-edge": [
#         "https://localhost:4566"
#       ],
#       "Authorization": [
#         "AWS4-HMAC-SHA256 Credential=LocalStackDummyAccessKey/20160623/us-east-1/apigateway/aws4_request, SignedHeaders=content-type;host;x-amz-date;x-amz-target, Signature=1234"
#       ]
#     },
#     "pathParameters": {},
#     "body": "",
#     "isBase64Encoded": false,
#     "resource": "/restapis/57p52fbjmh/test/_user_request_/resource42/",
#     "httpMethod": "GET",
#     "queryStringParameters": {},
#     "multiValueQueryStringParameters": {},
#     "requestContext": {
#       "path": "/test/resource42",
#       "accountId": "000000000000",
#       "resourceId": "nm51p2bmrw",
#       "stage": "test",
#       "identity": {
#         "accountId": "000000000000",
#         "sourceIp": "127.0.0.1",
#         "userAgent": "curl/7.68.0"
#       },
#       "httpMethod": "GET",
#       "protocol": "HTTP/1.1",
#       "requestTime": "2020-11-07T16:26:14.717Z",
#       "requestTimeEpoch": 1604766374717
#     },
#     "stageVariables": {
#       "answer": "42"
#     }
#   },
#   "context": {
#     "get_remaining_time_in_millis": 2928,
#     "function_name": "my-first-function",
#     "function_version": "$LATEST",
#     "invoked_function_arn": "arn:aws:lambda:us-east-1:000000000000:function:my-first-function",
#     "memory_limit_in_mb": "1536",
#     "aws_request_id": "d79e0143-e71d-14a9-bbe3-7bd6ec674499",
#     "log_group_name": "/aws/lambda/my-first-function",
#     "log_stream_name": "2020/11/07/[$LATEST]0608a4549ef5b00faea9eaddb434853b",
#     "identity": {
#       "cognito_identity_id": null,
#       "cognito_identity_pool_id": null
#     }
#   }
# }



def handler(event, context):
    data = {
        'event': event,  # will contains payload
        'context': {
            'get_remaining_time_in_millis': context.get_remaining_time_in_millis(),
            'function_name': context.function_name,
            'function_version': context.function_version,
            'invoked_function_arn': context.invoked_function_arn,
            'memory_limit_in_mb': context.memory_limit_in_mb,
            'aws_request_id': context.aws_request_id,
            'log_group_name': context.log_group_name,
            'log_stream_name': context.log_stream_name,
        }
    }
    if context.identity:
        data['context']['identity'] = {
            'cognito_identity_id': context.identity.cognito_identity_id,
            'cognito_identity_pool_id': context.identity.cognito_identity_pool_id
        }
    if context.client_context:
        data['context']['client_context'] = {
            'env': context.client_context.env,
        }
        if context.client_context.client:
            data['context']['client_context']['client'] = {
                'installation_id': context.client_context.client.installation_id,
            }

    return {
        'statusCode': 200,
        'body': data,
    }
