provider "aws" {
  region = "us-east-1"
  access_key = "test"
  secret_key = "test"
  # needed for localstack
  s3_force_path_style         = true
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id = true
  endpoints {
    dynamodb       = "http://localhost:4566"
    s3             = "http://localhost:4566"
    sqs            = "http://localhost:4566"
    lambda         = "http://localhost:4566"
    cloudwatch     = "http://localhost:4566"
    cloudwatchlogs = "http://localhost:4566"
    apigateway     = "http://localhost:4566"
  }

  version = "~> 3.13.0"
}
