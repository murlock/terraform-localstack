# note: it uses cloudwatchlogs for endpoint
# aws --endpoint http://localhost:4566 logs describe-log-groups --log-group-name /aws/lambda/my-first-function
# I can create a stream from this log_group
# aws --endpoint http://localhost:4566 logs create-log-stream --log-group-name /aws/lambda/my-first-function --log-stream-name plop
# TODO: how can I receive events ?
#
# each invoke of lambda create a new log-streams (RTFM ?)
# we can see them with
# aws --endpoint http://localhost:4566 logs describe-log-streams --log-group-name /aws/lambda/my-first-function
# Is it related to localstack since it spawns a new docker for each lambda invoke ?
# (check LAMBDA_EXECUTOR)

resource "aws_cloudwatch_log_group" "example" {
  name              = "/aws/lambda/${aws_lambda_function.my-function.function_name}"
  retention_in_days = 14
}