resource "aws_lambda_function" "my-auth-function" {
  function_name = "my-auth-function"
  filename = "function/my-authorizer.zip"
  handler = "authorizer.handler"
  # TODO implement IAM
  role = ""
  runtime = "python3.7"
  # used to trigger update, can we force creation of zip file in terraform ?
  source_code_hash = filebase64sha256("function/my-authorizer.zip")

  depends_on = [
    # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function#cloudwatch-logging-and-permissions
    # TODO when enabled, terraform detect a error on loop cycle
    # Error: Cycle: aws_cloudwatch_log_group.example, aws_lambda_function.my-function
    # but documentation refers to it
    # aws_cloudwatch_log_group.example,
    # TODO IAM
    # aws_iam_role_policy_attachment.lambda_logs,
  ]
}