# Variables
variable "myregion" {
  type = string
  default = "us-east-1"
}

variable "accountId" {
  type = string
  default = "000000000000"
}

# API Gateway
resource "aws_api_gateway_rest_api" "api" {
  name = "myapi"
}

resource "aws_api_gateway_resource" "resource" {
  path_part   = "resource42"
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}

# IT WORKS
resource "aws_api_gateway_method" "method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "GET"
  authorization = "NONE"
}

# FIXME: it doesn't work
resource "aws_api_gateway_integration" "integration" {
  # The API Gateway instance
  rest_api_id             = aws_api_gateway_rest_api.api.id
  # The PATH
  resource_id             = aws_api_gateway_resource.resource.id
  # Method
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.my-function.invoke_arn
}

resource "aws_api_gateway_deployment" "MyDemoDeployment" {
  depends_on = [aws_api_gateway_integration.integration]

  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = "test"

  variables = {
    "answer" = "42"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Lambda
#resource "aws_lambda_permission" "apigw_lambda" {
#  statement_id  = "AllowExecutionFromAPIGateway"
#  action        = "lambda:InvokeFunction"
#  function_name = aws_lambda_function.my-function.function_name
#  principal     = "apigateway.amazonaws.com"
#
#  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
#  source_arn = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.resource.path}"
#}


# Lambda is available with
# curl http://localhost:4566/restapis/{REST_API_ID}/{STAGE}/_user_request_/{RESOURCE}/